# coding=utf-8
from __future__ import unicode_literals

from django.db import models

# Create your models here.


class Pais(models.Model):
    class Meta:
        verbose_name = 'País'
        verbose_name_plural = 'Paises'

    nombre = models.CharField(max_length=254, verbose_name='nombre')

    def __unicode__(self):
        return self.nombre

class Departamento(models.Model):
    class Meta:
        verbose_name = 'Departamento'
        verbose_name_plural = 'Departamentos'

    nombre = models.CharField(max_length=254, verbose_name='nombre')
    pais = models.ForeignKey(Pais, verbose_name='País')

    def __unicode__(self):
        return self.nombre


class Ciudad(models.Model):
    class Meta:
        verbose_name = 'Ciudad'
        verbose_name_plural = 'Ciudades'

    nombre = models.CharField(max_length=254, verbose_name='nombre')
    departamento = models.ForeignKey(Departamento, verbose_name='departamento')

    def __unicode__(self):
        return self.nombre