from django.contrib import admin

# Register your models here.
from ubicacion.models import Pais, Departamento, Ciudad


@admin.register(Pais)
class PaisAdmin(admin.ModelAdmin):
    pass


@admin.register(Departamento)
class DepartamentoAdmin(admin.ModelAdmin):
    list_display = ('nombre','pais')
    list_display_links = ('nombre','pais')


@admin.register(Ciudad)
class CiudadAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'departamento', )
    list_display_links = ('nombre', 'departamento',)

