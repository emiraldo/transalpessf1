from django.contrib import admin

# Register your models here.
from usuario.models import Cliente, Usuario


@admin.register(Cliente)
class ClienteAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'apellido', 'cedula')
    list_display_links = ('nombre', 'apellido', 'cedula')

@admin.register(Usuario)
class UsuarioAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'apellido', 'cedula', 'tipo', 'usuario')
    list_display_links = ('nombre', 'apellido', 'cedula', 'tipo', 'usuario')
