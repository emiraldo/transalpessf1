# coding=utf-8
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models

# Create your models here.


class Cliente(models.Model):
    class Meta:
        verbose_name = 'Cliente'
        verbose_name_plural = 'Clientes'

    nombre = models.CharField(max_length=254, verbose_name='nombre')
    apellido = models.CharField(max_length=254, verbose_name='apellido')
    cedula = models.CharField(max_length=254, verbose_name='cédula')
    telefono = models.CharField(max_length=254, verbose_name='teléfono')
    direccion = models.CharField(max_length=254, verbose_name='dirección')

    def __unicode__(self):
        return self.nombre+" "+self.apellido

class Usuario(models.Model):
    class Meta:
        verbose_name = 'Usuario'
        verbose_name_plural = 'Usuarios'

    nombre = models.CharField(max_length=254, verbose_name='nombre')
    apellido = models.CharField(max_length=254, verbose_name='apellido')
    cedula = models.CharField(max_length=254, verbose_name='cédula')
    telefono = models.CharField(max_length=254, verbose_name='teléfono')
    direccion = models.CharField(max_length=254, verbose_name='dirección')
    tipo = models.CharField(max_length=254, choices=(
                            ('Administrador', 'Administrador'),
                            ('Operario', 'Operario')), verbose_name='tipo de usuario')
    usuario = models.ForeignKey(User)

    def __unicode__(self):
        return self.nombre+" "+self.apellido
