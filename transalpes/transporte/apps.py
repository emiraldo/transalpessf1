from __future__ import unicode_literals

from django.apps import AppConfig


class TransporteConfig(AppConfig):
    name = 'transporte'

from suit.apps import DjangoSuitConfig

class SuitConfig(DjangoSuitConfig):
    layout = 'vertical'