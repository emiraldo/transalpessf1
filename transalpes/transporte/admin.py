from django.contrib import admin

# Register your models here.
from transporte.models import Conductor, Vehiculo, Paquete, Ruta


@admin.register(Conductor)
class ConductorAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'apellido', 'cedula')
    list_display_links = ('nombre', 'apellido', 'cedula')

@admin.register(Vehiculo)
class ConductorAdmin(admin.ModelAdmin):
    list_display = ('conductor', 'placa', 'modelo')
    list_display_links = ('conductor', 'placa', 'modelo')

@admin.register(Paquete)
class PaqueteAdmin(admin.ModelAdmin):
    list_display = ('cliente_envio', 'cliente_destino', 'tipo')
    list_display_links = ('cliente_envio', 'cliente_destino', 'tipo')
    raw_id_fields = ('cliente_envio','cliente_destino')

@admin.register(Ruta)
class RutaAdmin(admin.ModelAdmin):
    list_display = ('paquete', 'origen', 'destino', 'vehiculo')
    list_display_links = ('paquete', 'origen', 'destino', 'vehiculo')
    raw_id_fields = ('paquete','origen','destino')
