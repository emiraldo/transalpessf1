# coding=utf-8
from __future__ import unicode_literals

from django.db import models

# Create your models here.
from geoposition.fields import GeopositionField

from ubicacion.models import Ciudad
from usuario.models import Cliente


class Conductor(models.Model):
    class Meta:
        verbose_name = 'Conductor'
        verbose_name_plural = 'Conductores'

    nombre = models.CharField(max_length=254, verbose_name='nombre')
    apellido = models.CharField(max_length=254, verbose_name='apellido')
    cedula = models.CharField(max_length=254, verbose_name='cédula')
    telefono = models.CharField(max_length=254, verbose_name='teléfono')
    direccion = models.CharField(max_length=254, verbose_name='dirección')

    def __unicode__(self):
        return self.nombre+" "+self.apellido

class Vehiculo(models.Model):
    class Meta:
        verbose_name = 'Vehículo'
        verbose_name_plural = 'Vehículos'

    conductor = models.ForeignKey(Conductor)
    placa = models.CharField(max_length=10, verbose_name='placa')
    modelo = models.CharField(max_length=254, verbose_name='modelo')
    tipo =  models.CharField(max_length=254, verbose_name='tipo')
    carga = models.CharField(max_length=254, verbose_name='carga')

    def __unicode__(self):
        return self.placa



class Paquete(models.Model):
    class Meta:
        verbose_name = 'Paquete'
        verbose_name_plural = 'Paquetes'

    tipo = models.CharField(max_length=10, verbose_name='tipo')
    alto = models.CharField(max_length=10, verbose_name='alto')
    ancho = models.CharField(max_length=10, verbose_name='ancho')
    largo = models.CharField(max_length=10, verbose_name='largo')
    peso = models.CharField(max_length=10, verbose_name='peso')
    cliente_envio = models.ForeignKey(Cliente, related_name='cliente_envio')
    cliente_destino = models.ForeignKey(Cliente, related_name='cliente_destino')

    def __unicode__(self):
        return "Paquete #"+str(self.id)

class Ruta(models.Model):
    class Meta:
        verbose_name = 'Ruta'
        verbose_name_plural = 'Rutas'

    paquete = models.ForeignKey(Paquete)
    origen = models.ForeignKey(Ciudad, related_name='ciudad_origen', verbose_name='Ciudad de origen')
    direccion_origen = GeopositionField(verbose_name='dirección de origen', blank=True, null=True)
    destino = models.ForeignKey(Ciudad, related_name='ciudad_destino', verbose_name='Ciudad destino')
    direccion_destino = GeopositionField(verbose_name='dirección destino', blank=True, null=True)
    vehiculo = models.ForeignKey(Vehiculo, verbose_name='Vehículo')
    vehiculo_ubicacion  = GeopositionField(verbose_name='ubicación del vehículo', blank=True, null=True)

    def __unicode__(self):
        return "Ruta para el paquete #"+str(self.paquete.id)